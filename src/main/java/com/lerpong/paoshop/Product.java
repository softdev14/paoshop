/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lerpong.paoshop;

import java.io.Serializable;


/**
 *
 * @author arpao
 */
public class Product implements Serializable {
    private String id;
    private String Name;
    private String Brand;
    private Double Price;
    private int Amount;
    private Double currentAllPrice;
    private int countProduct;

    public Double getCurrentAllPrice() {
        return currentAllPrice;
    }

    public void setCurrentAllPrice(Double currentAllPrice) {
        this.currentAllPrice = currentAllPrice;
    }

    public int getCountProduct() {
        return countProduct;
    }

    public void setCountProduct(int countProduct) {
        this.countProduct = countProduct;
    }

    public Product(String id, String Name, String Brand, Double Price, int amount) {
        this.id = id;
        this.Name = Name;
        this.Brand = Brand;
        this.Price = Price;
        this.Amount = amount;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String Brand) {
        this.Brand = Brand;
    }

    public Double getPrice() {
        return Price;
    }

    public void setPrice(Double Price) {
        this.Price = Price;
    }

    public int getAmount() {
        return Amount;
    }

    public void setAmount(int amount) {
        this.Amount = amount;
    }

    @Override
    public String toString() {
        return "id= " + id + ", Name= " + Name + ", Brand= " + Brand + ", Price= " + Price + ", amount= " + Amount;
    }
    
    
}
