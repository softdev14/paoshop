/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lerpong.paoshop;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author arpao
 */
public class ProductService {
    private static ArrayList<Product> productList = null;
    static {
        productList = new ArrayList<>();
        load();
    }

    public static boolean addProduct(Product product) {
        productList.add(product);
        save();
        return true;
    }
    
    public static boolean delProduct(Product product) {
        productList.remove(product);
        save();
        return true;
    }
    
    
    
    public static boolean delProduct(int index) {
        productList.remove(index);
        save();
        return true;
    }
    
    public static ArrayList<Product> getProducts() {
        return productList;
    }
    
    public static Product getProduct(int index) {
        return productList.get(index);
    }
    
    public static boolean updateProduct(int index, Product product) {
        productList.set(index, product);
        save();
        return true;
    }
    
    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {  
            file = new File("pao.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(productList);
            fos.close();
            oos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {  
            file = new File("pao.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            productList = (ArrayList<Product>) ois.readObject();
            fis.close();
            ois.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProductService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    static void delAllProduct(int index) {
        productList.removeAll(productList);
        save();
    }
    
    
}
